# Lovebud

_Update: Sinuous ended up being a perfect work-in-progress to contribute to_

Picking up where components in [Voko](https://gitlab.com/nthm/voko/tree/components)
left off. Focusing on state management and efficient rerendering. Designing a
new reviver that steps away from hyperscript and focuses on tag template
literals.

I've designed working component systems with Voko for previous projects, like
Relocate, but ran into many issues which eventually put them all on hold.

In Voko, there's no official way of communicating changes in state. Components
often export functions that know how to make changes to their live DOM nodes,
and they were called manually from elsewhere in the tree. If you updated state,
you needed to know who else might need to be pinged about that change.
Meanwhile, updating the DOM manually had no issues and I intend on continuing
that model in Lovebud. Instead, Voko suffered from not having a component
lifecycle or the ability to schedule work in side effects. For instance, there's
no way of knowing when the DOM node was connected to the tree, who mounted it,
or how many instances of a given component shared state.

Component lifecycles are heavily tied to state management, so I've been reading
a lot on that. In Vuex, I learned it embraces the mutation of state via Proxy
objects. Originally I focused on a Redux-like system, but realized they can get
away with the separation of state and UI from having reconciler - which I don't
want.

I want to focus on the subscriptions of changes to state for a primary
reactivity model.

Currently reverse engineering the parser of developit/htm. It's the only tag
template project I've found that doesn't drop the static markup into a template
tag and reparse its content (as done in hyperHTML and lit-html). It's a great
approach but not what I'm looking for.

Most of my notebooks are about this, so this repo will be mostly planning and
research.

## Goals

- Stay close to JS and native DOM APIs. Don't create a layer of abstraction
- Be able to run directly in a browser
- Server side rendering and hydration is the base, not an afterthought
- Reactivity tied to state like Vuex, _not_ separate like Redux
- No diffing renders at all
- Able to be optimized by Babel; specifically the parser as done in HTM
- Devs should name as few things as possible; inline components, HTML, and CSS
- Provide easy performance metrics on redraws and render times

## Parts

There's got to be a **parser** that can read HTML-like markup into either DOM
for the client or real HTML strings for SSR. To be modular/testable/good there
shouldn't be two parsers; one parser should be shared and communicate to writing
with some kind of intermediate tree. I know I said no virtual DOM but as long as
its not a fundemental concept and can be optimized out it's OK. The parser needs
to keep track of references and events as it evaluates the page lazily. With
SSR,these references will be given to the client with the tree already built and
it can use _querySelector_ to find them as needed.

Next there's the **state management**, which is the entirely of the reactivity
model for the framework. There are no rerenders (kinda). There's a Proxy()
object that holds values and a list of subscribers. Update logic is written
alongside the component and ties into the state tree.

Lastly there's the actual add/remove/replace of components to/from the DOM, and
calling the appropriate hooks like onmount. Also little tasks like handling
template caching and keeping track of metrics like how who is updating what and
what is being redrawn, if ever. This is the overall **lifecycle** of the app.

## Feel

This will _write_ a lot like a server side rendering framework that uses macros.
Like Jade. That gets you really far. I'm mostly trying to figure out how to
blend the hydration/passover to the client runtime.

In React, Mithril, Choo, etc, you drop literally all the native DOM APIs. You
have an abstracted world and get used to the declarative approach. Granted, it
maintains a simple/comprehensible structure that scales. It works well. It also
teaches a lot of things that rub you the wrong way if you know how vanilla web
rendering works. It hides base concepts like the DOM and event loop, but you
can't actually get away from that entirely... It keeps code simple by teaching
to redraw the world from scratch each change and having each component _be_ one
render method. There are some rules in the framework, like unidirectional flow
between components, immutable state, and avoiding side effects. These rules are
often the abstraction brushing up against the way vanilla JS and DOM actually
work. Encorporating other frameworks or vanilla HTML/JS alongside React (and
friends) is _possible_ but advised against and hardly done in practice because
it breaks the abstraction.

Whether or not browsers will bend/optimize to fit the abstraction is another
question, but for now I don't really want to pave over the web just to chase a
"better" developer experience. Adding layers leads to heavy bundles and rough
user experiences. Let's not.

Then again, after using React, try building a simple vanilla HTML/JS login app
of a few screens, some forms, buttons for transitioning, animations, an optional
2FA screen, etc. See how unbelievably messy the code becomes.

It's only slightly more simple with HTML-in-JS or _minimum_ components in a
system like Voko. I wrote Voko so my coworkers would stop `innerHTML`ing or
`document.write`ing alongside a pile of other bad practices. _The separation of
HTML and JS is hard._ The webdev ecosystem largely settled on HTML-in-JS to
simplify things, but using that exclusively leads to _a lot_ of work in JS on
startup. What else are you gonna do? Use a server rendering framework? Enjoy
hydrating that. Systems like Next and Gatsby are beautiful in theory,
questionable in the docs, and so wildly complex in practice that the blackhole
spawned by `npm install` scares off teams already too busy learning this month's
trending framework.

Anti-clientside-framework camps have opted for an entirely SSR approach; like
sr.ht or Tildes. There has to be somewhere in the middle though.

I'm trying to find that place of having SSR and still a clientside with
reactivity, components, and updates.

## Update 2020: Sinuous

I've been researching proxy state management and how to convey state trees
between client and server, all without considering much how to patch/update the
DOM. I figured I'd need to have components be a boundary, and I was stuck
deciding whether or not to even support "update()" as a "class property" of
sorts along with "create()" as a render method. In the post on conditional
rendering I even described the idea of subscribing (automatically in render) to
values in state so you could provide patch functions afterwards (!!) so tell the
state engine what to call instead of it telling you to rerender.

I've went deep into proxy state management and mutation tracking to see how
libraries intercept the countless mutable, loosely immutable, and safely
immutable prototype functions of Array, Map, Set, Object, Uint8ClampedArray, and
so on. It's not pretty, but I believed it was the right way to avoid diffing and
immutability.

Never did I even imagine it would be possible to hold references to slices of
DOM, including text nodes, and then having the rendering engine handle state
updates by using appendChild/insertBefore and friends to efficiently
add/insert/splice/clear DOM nodes within a live tree without diffing. No
diffing. Blindly splicing based on the state you're holding in memory on where
elements exist in the DOM in certain "groups" - not only parent/child groups but
also tagging elements with a special ".__g" property for keeping inventory.
That's what [Sinuous][1] does, and it's one most amazing thing I've ever read in
the hyperscript community, up with HTM's parser. The only issue I see so far is
lack of refs to DOM nodes - I'm used to writing directly to DOM myself but in
Sinuous it'd mess up the DOM pointers to do work outside the library.

It's still young, but it covers all grounds and builds off of the work from Ryan
Solid in their work on [Solid][2] and [DOM Expresions][3], and Adam Haile's
reactivity work in their UI library [Surplus][4] based on his their own
observability library [S.js][5]. All of these are libraries that take the right
approach to state, reactivity, and compilation. Even the project tagline is
"Embraces hyperscript and friends", which is truly a better way forward. The
hyperscript reviver in Dom Expresions is able to read the component and convert
it to native DOM operations that also provide patch/update functions. The JS
makes sense. In Sinuous, the observability engine (which is generic and can be
replaced by many libraries that use a similar API) takes this concept further
by allowing the user to swap the hyperscript (JSX/HTM) and observability/pub+sub
engines. There's even first class examples using Proxy() state stores.

I've read all the code and seen it mature for a while. I couldn't write
something better myself. That's really hard for me to say, because I want to
know my tools and create them myself to completely understand the stack. I've
scoured this project for everything I can improve on, specifically the
state/observability engine which I was certain could be improved by using
proxies and state mutation tracking to avoid requiring immutability, but after a
lot of notes and trying out how it felt in prototypes, sending a value into a
pipe to queue an update makes the most sense. It's a "commit" instead of trying
to autodeploy each write. There may be some improvements that can be made, but
it'll likely be in my own implementations outside of Sinuous. As a framework,
they've made very good unopinionated decisions that don't weigh developers down
(or users, actaully); size is a priority and the author's into code golf. They
even have hydration done using a novel but promising approach.

There's still areas to explore. I'll need mutation tracking and multiplayer
client-server state sync. I'll need to have performance metric collection on
updates and patch rendering times to let people know what's expensive in the
application. Sinuous supports Typescript but is entirely JS (for very valid code
golf and trust-me-I-know-what-I'm-doing reasons that the compiler will not be
able to understand). I'll need to figure out a way of maintaining state in
Typescript so type checking is performed on components at compile time.
Immutability isn't required in Sinuous, since the observable engine is really
just a series of interdependent tubes; you can put anything down them including
the previous value to cause a recalculation. Sinuous uses closures to prevent
eager evaluation of state values, which would otherwise cause subscriptions to
miss their association to a component, but I'm not sure they've tried using
``love('AvatarIcon')`<img alt=${observableName()} />`` to be more explicit (to
Typescript, mostly) the type the observable returns, and about registering
components into a global tracking store - not that they'd have any reason to
want that (unless as dev tools like I do). Tag template literals, if you're
using them via HTM, also have this magic property of being the same object that
allows them to be potentially more performant when used correctly - could be
researched but templates (in Sinuous) are likely even better as they take the
approach of hyperHTML and HTML template tag cloning.

[1]: https://github.com/luwes/sinuous
[2]: https://github.com/ryansolid/solid
[3]: https://github.com/ryansolid/dom-expressions
[4]: https://github.com/adamhaile/surplus
[5]: https://github.com/adamhaile/S
