# Chasing better type support in Sinuous observable

Looking at this date picker component that uses Sinuous

https://codesandbox.io/s/sinuous-date-picker-thxdt?file=/src/date-picker.js:1792-1800

Not very happy with it, because it's hard to understand the types throughout the
components/project. Here are some fake snippets I've annotated:

```ts
const weeks = computed(() => {
    // Work
    // Work
    // It's not clear if days() is part of the pubsub or not
    return 10 * days()
})

// It's also confusing in situations like
const view = html`
  <table class="date-picker">
    <tr>
      <td class="date-picker-btn" onClick=${() => go(-1)}>◀</td>
      <!-- Versus, less function calls?
      <td colSpan=5>${month} ${year}</td>
      -->
      <td colSpan=5>${() => `${month()} ${year()}`}</td>
      <td class="date-picker-btn" onClick=${() => go(+1)}>▶</td>
    </tr>
    <tr>
      <!-- NOT updating a pubsub even though it looks like we are -->
      ${days.map(day => html`<th>${day}</th>`)}
    </tr>
    <!-- Now "week" has no type definition, because weeks() is "() => any" -->
    ${() => weeks().map(week => html`
      <tr>
        ${week.map(day => html`
          <!-- It's not 100% clear we ARE updating a pubsub (but not for date) -->
          <td class="date-picker-btn ${day.class}" onClick=${() => { offset(0); value(day.value) }}>${day.date}</td>
        `)}
      </tr>
    `)}
  </table>
`
```

In particular, the code for the date involves creating 6 Date() objects ever
render... The values passed through the observables are strings (ISO values of
the date objects

Below is some hackhack I did in TS trying to check the vibe:

```ts
let proxyTracking = false
const proxyTree: Tree = () => proxyTree
type Tree = {
    [prop: string]: any
    (): Tree
}
const s = new Proxy<Tree>(proxyTree, {
    // State wiring logic...
    get: function(target, prop, receiver) {
        if (typeof prop === 'symbol' || typeof prop === 'number') return
        console.log(`Get ${prop}`, proxyTracking ? 'while tracking' : '')
        return proxyTree[prop]
    },
    set: function(target, prop, value) {
        if (typeof prop === 'symbol' || typeof prop === 'number') return false
        console.log(`Set ${prop}=${value}`, proxyTracking ? 'while tracking' : '')
        proxyTree[prop] = value
        return true
    },
    apply: function(target, thisArg, argumentsList) {
        console.log(`Tracking`);
        proxyTracking = true;
        return proxyTree;
    }
})

// Property 'days' does not exist on type '() => Tree'.ts(2339)
s().days = 15
// Regardless, s.days will be "any" unless "type Tree" has "days: number"
s().weeks = s.days * 10
```

It compiles (!). Doesn't have the desired type hints though...

Now... This `s()` object can kind of be a "context":

TODO: How to know when they're done tracking? Does that ever close? At least in
systems that use closures it's obvious. What does that even mean, to close? I'm
considering `await` and "threads" (where multiple contexts may exist at once)

```ts
const context = s()
```

Now... `context.days` vs `s.days` ? Is context write only and s read-only? This
sounds a lot like React's hooks (which I haven't used yet)

Speaking of, the way React initializes the types for state, so "setState()"
understands what types are valid...Might be needed to init the type like that?

```ts
s({
    days: 18 + s.weeks
})
```

Likely will need a double call, if not a closure: First to track, then to set.

```ts
s()({
    // ...
})
```