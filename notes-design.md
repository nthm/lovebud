# Design

These are historic notes. I've since decided to use Sinuous as a UI library, see
readme.md, but I'll consolidate _some_ notes here into one file for reference.

## Parts

In the readme I mention there is a _parser_, a _state management system_, and
_component lifecycle system_.

### Parser

I've seen since HTM develop into a very well engineered parser that can even
emit JSON trees to be consumed and optimized out by Babel. In Summer 2019 I
really wanted to understand how the parser worked and to eliminate children
within components, which was a design decision to help remove some confusion
about component usage and lack of typing support often seen in the React
community. This is probably better as a design pattern or ESLint rule rather
than part of the parser, since normal HTML can obviously have children and I'm
very pro- reducing the seperation between JS components and standard HTML.

In the appendix there's annotations on the HTM parser codebase (it was in active
development when I was looking it over and has now changed).

### State

I was very into trees and Flux-inspired state stores. I was _positive_ that
state needed to be kept in a dedicated box, but diverged at immutability - I
never got into that and aligned more with proxy-based mutation tracking.

Here's a Proxy()-based state tree used in Overmind, an engine by Christian
Alfoni. Has some neat features: It tried to reuse Proxy objects (since they're
expensive) and you have enable/disable tracking for different trees. It uses a
function called "Proxyfier" to track _a few_ prototype methods of Array like
push and pop.

- [GitHub isolated proxy-state-tree](https://github.com/christianalfoni/proxy-state-tree)
- [GitHub within Overmind](https://github.com/cerebral/overmind/blob/next/packages/node_modules/proxy-state-tree/src/Proxyfier.ts)
- [Codesandbox demo](https://codesandbox.io/embed/lpmv68r8y9)

Here are two others.

Hyperactiv is nice and lightweight. It's pretty basic. Support computed
properties that are recomputed _every_ change on their dependency, regardless if
anyone's listening. Has a "batch" mode if you need to spam the Proxy.

Meanwhile proxy-watcher tries to handle _every possible prototype_ of Array,
Map, Set, TypedArray, Promise, Date, RegExp, etc. It breaks them into catagories
on whether they directly mutate the data, potentially allow for mutations (like
with `.forEach()`), or are safe/immutable. It's interesting, but also +60kb.

- [GitHub for Hyperactiv](https://github.com/elbywan/hyperactiv/)
- [GitHub for proxy-watcher](https://github.com/fabiospampinato/proxy-watcher)

These are the libraries inspiring Sinuous

- [GitHub Solid](https://github.com/ryansolid/solid)
- [GitHub S.js](https://github.com/adamhaile/S)

Here's some neat state/sync links:

- [Statebus.js](https://stateb.us/universal-sync)
- [Vue.js Reactivity API PR#22](https://github.com/vuejs/rfcs/pull/22)

### Lifecycle

Not needed. Here's "Disconnected" which is a MutationObserver if you really need
it, but, you probably won't since you can have reactivity via state and
component mounting/unmounting is likely determined by events or state activity
within your control.

## Appendix

Here's all the code snippets.

### HTM

First the HTM parser I tried to port for Lovebud.

```ts
const MODE_AFTER_SLASH = 'MODE_AFTER_SLASH';
const MODE_TEXT = 'MODE_TEXT';
// WHITESPACE is used while an attribute is being discovered, but hasn't reached an `=` yet
// If an `=` isn't reached, and a > or ${} is, then that's handled in commit() as an attribute
const MODE_ATTRIBUTE = 'MODE_ATTRIBUTE';
const MODE_AFTER_OPEN = 'MODE_AFTER_OPEN';

function love(statics: TemplateStringsArray, ...dynamics: unknown[]): Elementish {
  let mode = MODE_TEXT;
  let buffer = '';
  let quote = '';
  // The current node
  let element: Elementish = undefined;
  // If outside of a DOM environment, there needs to be a stack to refer to the parent node
  // let stack = [];
  let char: string;
  let attributeName: string;
  const selectorId = '';
  const selectorClass = '';

  // TODO: When does a commit NOT do anything useful...
  // In MODE_ATTRIBUTE
  const useBuffer = (field?: any) => {
    if (mode === MODE_TEXT && (field || (buffer = buffer.replace(/^\s*\n\s*|\s*\n\s*$/g, '')))) {
      if (!element) {
        element = document.createDocumentFragment();
      }
      element.appendChild(field ? dynamics[field] : buffer);
    }
    else if (mode === MODE_AFTER_OPEN && (field || buffer)) {
      // TODO: When using dynamics[field] if the tag is a function, then it needs to be called...
      // That disturbs the order of things...
      // Will need to collect all the children into a fragment? Pass to the component...
      // Not looking for JSX interoperability necessarily
      element.appendChild(element = document.createElement(field ? dynamics[field] : buffer));
      mode = MODE_ATTRIBUTE;
    }
    // Hot exit for everything else
    else if (mode !== MODE_ATTRIBUTE) { }

    else if (buffer === '...' && field) {
      if (!(field instanceof Object)) {
        throw new TypeError("Field isn't an object when using `...${}`");
      }
      for (const name in field) {
        setAttr(name, field);
      }
    }
    // This allows both with equals (i.e href="${}") or without (i.e onClick${})
    // Since `...` is handled above set the props
    else {
      setAttr(buffer, field);
    }
    buffer = '';
  };

  const setAttr = (name: string, value: any) => {
    if (!(element instanceof HTMLElement)) {
      throw new TypeError('No element to set attribute for');
    }
    if (name === 'class' || name === 'className') {
      element.classList.add(value);
    }
    if (name === 'ref') {
      value(element);
      return;
    }
    if (name === 'style') {
      if (!value || typeof value === 'string') {
        element.style.cssText = value || '';
        return;
      }
      if (value && typeof value === 'object') {
        for (const [k, v] of Object.entries(value)) {
          element.style[k] = v;
        }
      }
      return;
    }
    if (name[0] == 'o' && name[1] == 'n') {
      const event = name.toLowerCase().substring(2);
      element.addEventListener(event, value);
      return;
    }
    // Use `value || true` for `<input disabled>`
    name in element
      ? element[name] = value || true
      : element.setAttribute(name, value || true);
  };

  for (let i = 0; i < statics.length; i++) {
    // If a new static and one has already been processed
    if (i) {
      if (mode === MODE_TEXT) {
        useBuffer();
      }
      // For time when <a href=${}> yields static: `<a href=`, then `>`. So commit ${}...
      useBuffer(i);
    }

    for (let j = 0; j < statics[i].length; j++) {
      // Benchmarked to be faster than charAt()
      char = statics[i][j];
      if (mode === MODE_TEXT) {
        if (char === '<') {
          // There could be a buffer so use it before starting a new inline/child tag
          useBuffer();
          mode = MODE_AFTER_OPEN;
        }
        // Hot exit
        else {
          buffer += char;
        }
      }
      // Everything after here is _not_ MODE_TEXT
      // Regexless version of find and ignore matching quote
      else if (quote) {
        if (char === quote) {
          quote = '';
        }
        // Hot exit
        else {
          buffer += char;
        }
      }
      else if (char === '"' || char === "'") {
        // This falls for edge cases like <input placeholder=It's>... yields "Its"
        quote = char;
      }
      else if (char === '>') {
        // Done processing <tag attr> or <tag attr=value>
        useBuffer();
        mode = MODE_TEXT;
      }
      // Ignore everything until the tag ends via above block
      else if (mode === MODE_AFTER_SLASH) { }

      else if (char === '=') {
        mode = MODE_ATTRIBUTE;
        attributeName = buffer;
        buffer = '';
      }
      else if (char === '/') {
        useBuffer();
        element = element.parentElement;
        mode = MODE_AFTER_SLASH;
      }
      else if (char === ' ' || char === '\t' || char === '\n' || char === '\r') {
        // When mode is not MODE_TEXT so it's meaningful whitespace
        // This commits tag names and the value for a `name=value` attribute
        useBuffer();
        mode = MODE_ATTRIBUTE;
      }
      else {
        buffer += char;
      }
    }
  }
  useBuffer();

  return element;
}
```

### State

I had some notes on state trees in Typescript, which you'll be able to find in
the repository's history under state.ts. It was centered around finding ways to
enforce state keys, like Redux, in Typescript mostly with enums. Times have
changed and it's less relevant now.

### Writing components with a creator function

Next, I was trying to figure out the way of writing components. Here's a
function that supports both a class-like body or function body, similar to
React, allowing for dedicated render and patch methods. Updates/Patches were
supposed to use references (ref) to apply DOM changes manually.

```ts
interface LocalAppState {
  name: string;
  items?: string[];
}

const state = new Proxy<LocalAppState>({
  name: 'Default name',
  items: [],
}, {
    get(target: LocalAppState, key: keyof LocalAppState) {
      return target[key];
    },
    set(target: LocalAppState, key: keyof LocalAppState, value: any) {
      return true;
    },
  });

// I wanted to be able to track the refs, only because that's has been an
// important part of updates when writing in voko
interface ComponentData {
  ref?: Element[];
  // events?: Event[]; ?
}

const componentMap = new WeakMap<Element, ComponentData>();

const NavBar = createComponent({
  create(props: object) {
    return love`Content here <em>${state.name}</>` as HTMLInputElement;
  },
  update(stateKey) {
    // Handle switch case here depending on the state key that was modified
    // So `state.pub('followerCount', 55)` would call this with `followerCount`
    return true;
  },
});

const FormList = createComponent(() => {
  return document.createElement('body');
});
```

This was to get around the parameters being eagerly evaluated such as calling a
proxy item in `` love`Hello ${proxy.someValue}.` `` missing the opportunity for
the engine to notice that the state is being used by the component, since
parameters are evaluated _before_ the `love` function call.

In the end I just needed to do `` love()`...` `` and it was fine. I'll still
look into that for Sinuous.

### Writing components with chaining

Here was another idea, using a less appealing chaining approach:

```ts
// This is a simple component only implementing a render (no update)
// Don't worry about :: it was because I hated children
const ListItem =
    ({ text }: { text: string }, ...children: string[]) =>
        love`<li::${text}/>`;

// Function with chained-style love`` call
const SomeComponent: Lovable = () => love
  .register(SomeComponent)
  .defineLive({} as {
    // Me trying to make the TS compiler happy and give me type hints...
    ReferenceElement: HTMLElement;
  })
  // Manual subscriptions to items in the state tree (!!!)
  .sub(state, ['Name', 'Items'])
  `
  <h1.something#special>Hey ${state.Name}/>
  <!-- TODO: What does it mean here for subsequent calls to 'love' and not 'html' -->
  <.classedDiv>${state.Items.map((text) => love`
    <${ListItem} ${{ text }}/>
  `)}/>
  <p ref${love.live.ReferenceElement}>
    This is a ref'd paragraph appearing in \`SomeComponent.live\`
  </>`;

// Extensions
SomeComponent.update = (key: StateType, details) => {
  const { live } = love(SomeComponent);
  switch (key) {
    // TS will flag a string that's not a StateType
    case 'Name': {
      // Patch the DOM in a specific way that relates to the state field...
      live.ReferenceElement.textContent = state.Name;
    }
    // TODO: How to fallback to render()?
    // Involves rebinding to the parent node, whatever that may be for the particular instance
  }
};

// Function that requires setup before `return`
const AnotherComponent: Lovable = () => {
  const instance = love.register(AnotherComponent);
  const { live } = instance;
  // TODO: Figure out how to use conditional types in Typecript to set up the
  // type of `live`
  return instance`
    <h1 ref${live.Header}>${state.Name}</>
  `;
};
```

The comment:

> // TODO: How to fallback to render()? \
> // Involves rebinding to the parent node, whatever that may be for the
> particular instance

Is **huge**. How _do_ you even have a mental model of where a component is mounted
within an entire DOM tree and understand how to unmount, recreate, and remount
DOM to the same parent. The engine would have a lot of WeakMaps or UUIDs.

Neither took off because it's impossible to anticipate all the combinations
of state keys (not to mention if/when they change the primary key value) so a
list of followers may be split in the state tree by friend name, or username,
but without a UUID then the key could change, and `update()` calls would need to
do regex pattern matching (think Express.js route matching) in order to handle
generic state modifications - oof.

### Types

I can't even read some of the Typecript anymore, but it was about trying to help
the developer without prop types.

```ts
export interface Store<State> {
  state: ProxyHandler<{ [K in keyof State]?: State[K] }>;
  // TODO: They shouldn't need to define this. It should be a built in
  subscribe?: (c: Component, keys: [keyof State][]) => () => void;
}

export interface Component {
  // create<T>(creator: T): T extends (...args: any[]) => infer U ? U : never;
  create: (...args: any[]) => Element;
  update: () => boolean;
  // TODO: Link update switch cases to sub[] so all cases are covered
  // This is done in Artsy bookmark on 'Conditional Types'
  // Is there a way to infer sub[] from the switch cases of update()?
  // sub: [keyof State][];
}

type ReturnElementTypeFromCreate<T> =
  T extends (...args: any[]) => infer U ? U :
  T extends Component ? ReturnType<Component['create']> :
  Element | DocumentFragment;

type Creator = (...args: any[]) => (Element | DocumentFragment);
type ComponentCreator = Creator | { create: Creator };
export function createComponent<T extends ComponentCreator>(creator: T):
  T extends (...args: any[]) => infer U ? U :
  T extends { create: (...args: any[]) => infer U } ? U :
  never {
  // TODO: Does this return a function that tracks the placement of a component
  // instance in the DOM tree and try to reuse the DOM node for re-create()
  // calls? Hard to do. Parameters can't be cached either.

  // Regardless, this should be a function wrapper somehow. It needs to return
  // the element node type that TypeScript can display to be meaningful
  if (creator instanceof Function) {
    return creator;
  }
}

export interface Lovable {
  update?(modifiedStateKey: string, details?: object | string): void;
  live?: {
    [key: string]: HTMLElement | undefined
  };
}

// love = require('htm').bind(require('voko').v)
export function love(markups: TemplateStringsArray, ...dynamics: unknown[]): Element {
  return document.createElement('body');
}
// For the chaining-based approach
love.register = (componentFunction: Lovable) => love;
love.defineLive = (live: object) => love;
love.sub = (stateProxy: State, key: StateType[]) => love;
// TODO: How to define this dynamically per-component?
love.live = {};
```
