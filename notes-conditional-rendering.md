# Conditional Rendering

In VDOM-based situations a diffing algortihm can figure out what changes to make
to children, attributes, and entire nodes in as efficient way as possible. Since
the VDOM is recalculated from stratch each rerender, it's possible to write
things like

```jsx
<div>
  { loading && <p>Loading</p> }
  <p>Maybe a list</p>
  { loaded
    ? <List items={items}/>
    : <p>No content</p>
  }
</div>
```

Where `&&` and `? :` are meaningful since rerenders will always invoke the
expression. I'm trying to avoid diffing/VDOM so running through the entire
component isn't really an option except for the initial building - and in those
cases, if the expression is false then the component is entirely lost.

I'm ignoring the case where an unhandled update causes a full rerender. Let's
assume that doesn't happen since it should be treated as a fallback you'd write
yourself to know how messy it is to do an unmount and rebuild.

To avoid losing the component into the void, it would need to be defined outside
of the expression, and then referenced from inside the conditional. The key
point is to _define but not execute_ since execution could be full of side
effects and basically, it's work. There's a whole other story about how to be
lazy in JS, and it usually involves automatic "thunk"-ing or proxy objects.
There are ways. For now, I want to figure out how to have a component
registration that allows for boolean and ternary expressions

Example component:

```js
const { pidgeons } = state

const Component = lust`
  <h1>Title</h1>
  <p>There are ${pidgeons.length} pidgeons</p>
  <!-- This is the issue... -->
  ${pidgeons.length && lust`
    <div class${...}>
      <h2>Beautfil pidgeons</h2>
      ${pidgeons.sort(...).map(bird =>
        lust`<p>${capitalize(bird.name)}</p>`
      )}
    </div>
  `}
`
```

By using the boolean operator, the information could be lost unless you
rerender, which we'll assume is expensive.

Instead, you need to define outside the component:

```js
const { pidgeons } = state

const PidgeonList = () => lust`
  <div class${...}>
    <h2>Beautiful pidgeons</h2>
    ${pidgeons.sort(...).map(bird =>
      lust`<p>${capitalize(bird.name)}</p>`
    )}
  </div>
`

const Component = () => lust`
  <h1>Title</h1>
  <p>There are ${pidgeons.length} pidgeons</p>
  ${pidgeons.length && lust`<${PidgeonList()}>`}
`
```

This is bad for a few reasons

- `PidgeonList` is highly tied to the component/file. Named components should
  only be general/reusable; this one isn't. It's almost like it ought to be
  written directly in the parent :)
- The invokation of the component uses `lust``\` to be consistent, but is
  otherwise useless and just adds overhead.
- You have to name something. Ugh.
- You still need to define an update and do refs, but now you need to consider
  two places to sync updates.

The best solution takes a few ideas from Vue/Angular approaches similar to an an
attribute like `ngIf`. Now, attributes are only ever for HTML/CSS, since it's
confusing otherwise. Instead, do this:

```js
const { pidgeons } = state

const Component = () => lust`
  <h1>Title</h1>
  <p>There are ${pidgeons.length} pidgeons</p>
  ${Switch(
    () => pidgeons.length > 0,
    lust`
      <div class${...}>
        <h2>Beautiful pidgeons</h2>
        ${pidgeons.sort(...).map(bird =>
          lust`<p>${capitalize(bird.name)}</p>`
        )}
      </div>
    `
  )}
`
```

Where `Switch` is a component, and therefore a DOM node, that acts as a mount
point for another component, or optionally swap out with another component,
based on a conditional. i.e `Switch(() => list.length, List, "No items")`.
**Eager evaluation is dangerous** since if the component isn't actually being
rendered then it shouldn't have data associated with it, like how the `.map()`
above does. This is an unrelated problem, though.

This way there's a subscription to the proxy object, the component gets
registered, and the update flow is setup to replace/swap the last two arguments
when the condition is false. I imagine if you needed a complex conditional you
could setup a state _selector_ with its own hook and sub list.

Keep in mind that all this does is simplify some of the rewiring for you. It's
not magic.

## Updates

I've mentioned an update flow and needing to define an update, but what's that?

Originally I had been indoctrinated (by React and friends) into the idea of
rerendering. Even in the project readme.md I write that I'm researching
efficient rerendering. What I meant was wanting to shortcut rerenders by instead
providing update logic that can directly manipulate the DOM. You'd define update
logic for different state variables and they'd run when values change. Sometimes
you enter a state where it makes more sense to rerender because mutations would
be too hard by hand, or you're lazy, or developing/testing. There was even a
concept of subscribing to state variables and if there weren't update paths
defined then fallback to a rerender. Subscription even happened automatically if
you _read_ from them during your render... Maybe I learned that in Vuex.
However, what happens high up the tree? You'd kill all your children. Instead,
components were going to be a rerender boundary. This opened a whole other
problem of passing props to children, and what if props change, are parent
load/update times reflective of how many children they influence?

I have a lot of notes on that. Maybe I'll post them someday. It's a mess.

I'm not that convinced rerendering is worth it. The SSR-camp would tell you its
overrated. Similarly, defining the list of state and prop updates from within a
component is weird. You'll never write `myComp.update('state/counter', 5)`. The
state engine will just call the hook. You give it a patch function to call and
its closure is your isolation.

The render method is _only ever_ for the inital render.

Deciding on that removes a whole class of problems.
